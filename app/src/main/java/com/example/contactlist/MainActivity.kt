package com.example.contactlist

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val shared = getSharedPreferences("Preferences", MODE_PRIVATE)
        val sList = shared.getString("List", null)
        val gson = Gson()
        val type = object : TypeToken<List<Contact>>() {}.type
        val autoFill: MutableList<Contact> = if (sList != null)
            gson.fromJson(sList, type)
        else mutableListOf()
        val save: Button = findViewById(R.id.save)
        val tvName: EditText = findViewById(R.id.tvName)
        val tvNumber: EditText = findViewById(R.id.tvNumber)
        val listM: MutableList<Contact> = autoFill

        save.setOnClickListener {
            val edit = shared.edit()
            val name: String = tvName.text.toString()
            val number1: String = tvNumber.text.toString()
            val number = number1.toInt()
            val contact = Contact(
                name, number
            )
            val list = mutableListOf<Contact>()
            list += contact
            listM += list
            //val gson = Gson()
            val json = gson.toJson(listM)

            edit.apply {
                putString("List", json)
                apply()
            }
            Intent(this, ContactList::class.java).also {

                startActivity(it)
            }
            Toast.makeText(this, "Contact Saved", Toast.LENGTH_SHORT).show()
        }
    }
}