package com.example.contactlist

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import itemAdapter.ItemAdapter

class ContactList : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contack_list)

        val gson = Gson()
        val sharedPreferences: SharedPreferences = getSharedPreferences("Preferences", MODE_PRIVATE)
        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val data = sharedPreferences.getString("List", null)
        val type = object : TypeToken<List<Contact>>() {}.type
        val contact: MutableList<Contact> = gson.fromJson(data, type)
        val contactList = mutableListOf<Contact>()
        contactList += contact

        recyclerView.adapter = ItemAdapter(this, contactList)
    }
}